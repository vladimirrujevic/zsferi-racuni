package com.zsferi.racuni.ui.home


import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.os.Bundle
import android.util.Log
import android.util.Size
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.*
import androidx.core.content.ContextCompat
import android.os.Environment
import androidx.lifecycle.LifecycleOwner
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.text.FirebaseVisionCloudTextRecognizerOptions
import com.zsferi.racuni.R
import kotlinx.android.synthetic.main.fragment_home.*
import java.io.File
import java.util.*
import java.util.concurrent.Executors

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment(), LifecycleOwner {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (ContextCompat.checkSelfPermission(
                activity as AppCompatActivity,
                Manifest.permission.CAMERA
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            camera_preview.post { startCamera() }
        } else {
            Snackbar.make(
                camera_preview,
                getString(R.string.no_permission_camera),
                Snackbar.LENGTH_INDEFINITE
            ).show()
        }

        camera_preview.addOnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
            updateTransform()
        }
    }

    private val executor = Executors.newSingleThreadExecutor()

    private fun startCamera() {
        val previewConfig = PreviewConfig.Builder().apply {
            setTargetResolution(Size(640, 640))
        }.build()

        val preview = Preview(previewConfig)

        preview.setOnPreviewOutputUpdateListener {
            val parent = camera_preview.parent as ViewGroup
            parent.removeView(camera_preview)
            parent.addView(camera_preview, 0)
            camera_preview.surfaceTexture = it.surfaceTexture
        }

        val imageCaptureConfig = ImageCaptureConfig.Builder()
            .apply {
                setCaptureMode(ImageCapture.CaptureMode.MIN_LATENCY)
            }.build()
        val imageCapture = ImageCapture(imageCaptureConfig)
        btnCapture.setOnClickListener {
            val file = File(
                context?.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                "${System.currentTimeMillis()}.jpg"
            )

            imageCapture.takePicture(file, executor,
                object : ImageCapture.OnImageSavedListener {
                    override fun onError(
                        imageCaptureError: ImageCapture.ImageCaptureError,
                        message: String,
                        exc: Throwable?
                    ) {
                        val msg = "${getString(R.string.event_capture_failed_message)} $message"
                        Log.e("CameraXApp", msg, exc)
                        camera_preview.post {
                            Snackbar.make(camera_preview, msg, Snackbar.LENGTH_LONG).show()
                        }
                    }

                    override fun onImageSaved(file: File) {
                        val msg = getString(R.string.event_new_receipt);
                        Log.d("CameraXApp", msg)
                        camera_preview.post {
                            Snackbar.make(camera_preview, msg, Snackbar.LENGTH_LONG).show()
                            newReceipt(file.absolutePath)
                        }
                    }
                })
        }

        CameraX.bindToLifecycle(this, preview, imageCapture)
    }

    private fun newReceipt(path: String) {
        val file = File(path)
        if (file.exists()) {
            val bmap: Bitmap = BitmapFactory.decodeFile(file.absolutePath)
            val image: FirebaseVisionImage = FirebaseVisionImage.fromBitmap(bmap)
            val textR_options = FirebaseVisionCloudTextRecognizerOptions.Builder()
                .setLanguageHints(Arrays.asList("en", "sl"))
                .build()
            val textRecognizer = FirebaseVision.getInstance().getCloudTextRecognizer(textR_options)
            textRecognizer.processImage(image)
                .addOnSuccessListener {
                    val text = it.text.toString()
                    val blocks = it.textBlocks
                    val db = FirebaseFirestore.getInstance()
                    val date = Date()
                    db.collection("racuni/").add(
                        mapOf(
                            "store" to blocks[0].text.toString(),
                            "text" to text,
                            "path" to path,
                            "time" to date.time
                        )
                    ).addOnSuccessListener {
                        Snackbar.make(
                            camera_preview,
                            getString(R.string.event_receipt_saved),
                            Snackbar.LENGTH_LONG
                        ).show()
                    }.addOnFailureListener {
                        Snackbar.make(
                            camera_preview,
                            it.localizedMessage!!.toString(),
                            Snackbar.LENGTH_LONG
                        ).show()
                    }
                    Log.println(Log.INFO, "firebase_text", "${it.text.toString()} $path")
                }
                .addOnFailureListener {
                    val msg = "${it.localizedMessage}"
                    Snackbar.make(camera_preview, msg, Snackbar.LENGTH_LONG).show()
                }
        }
        Log.println(Log.INFO, "cameraPath", path)
    }

    private fun updateTransform() {
        val matrix = Matrix()

        // Compute the center of the view finder
        val centerX = camera_preview.width / 2f
        val centerY = camera_preview.height / 2f

        // Correct preview output to account for display rotation
        val rotationDegrees = when (camera_preview.display.rotation) {
            Surface.ROTATION_0 -> 0
            Surface.ROTATION_90 -> 90
            Surface.ROTATION_180 -> 180
            Surface.ROTATION_270 -> 270
            else -> return
        }
        matrix.postRotate(-rotationDegrees.toFloat(), centerX, centerY)

        // Finally, apply transformations to our TextureView
        camera_preview.setTransform(matrix)
    }


}

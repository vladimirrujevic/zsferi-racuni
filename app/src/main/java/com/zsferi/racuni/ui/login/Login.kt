package com.zsferi.racuni.ui.login


import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.zsferi.racuni.MainActivity

import com.zsferi.racuni.R
import kotlinx.android.synthetic.main.fragment_login.*

class Login : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnLogin.setOnClickListener {
            val email = txtEmail.text.toString()
            val password = txtPassword.text.toString()
            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                .addOnSuccessListener {
                    startActivity(Intent(context, MainActivity::class.java))
                    (activity as LoginActivity).closeActivity()
                }
                .addOnFailureListener {
                    Log.println(Log.INFO, "theApp", it.toString())
                    Snackbar.make(login_fragment, it.localizedMessage as CharSequence, Snackbar.LENGTH_LONG).show()
                    //Toast.makeText(context, "Prijava neuspešna", Toast.LENGTH_LONG).show()
                }
        }
    }

}

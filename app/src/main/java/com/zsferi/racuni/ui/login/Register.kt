package com.zsferi.racuni.ui.login


import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.zsferi.racuni.MainActivity

import com.zsferi.racuni.R
import kotlinx.android.synthetic.main.fragment_register.*

class Register : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnRegister.setOnClickListener {
            val email = txtEmail.text.toString()
            val password = txtPassword.text.toString()
            val password2 = txtPassword2.text.toString()

            if (password != password2) {
                txlPassword2.error = getString(R.string.error_passwords_match)
            } else {
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                    .addOnSuccessListener {
                        startActivity(Intent(context, MainActivity::class.java))
                    }
                    .addOnFailureListener {
                        //Toast.makeText(context, it.localizedMessage, Toast.LENGTH_SHORT).show()
                        Snackbar.make(register_fragment, it.localizedMessage as CharSequence, Snackbar.LENGTH_LONG).show()
                    }
            }
        }

        txtPassword2.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if(txtPassword.text.toString() != txtPassword2.text.toString()) {
                    txlPassword2.error = getString(R.string.error_passwords_match)
                    btnRegister.isEnabled = false
                } else {
                    txlPassword2.error = ""
                    btnRegister.isEnabled = true
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
    }


}

package com.zsferi.racuni.ui.receipts

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zsferi.racuni.R
import com.zsferi.racuni.vao.Receipt

class ReceiptListAdapter(var receipts: ArrayList<Receipt>, var context: Context)
    :RecyclerView.Adapter<ReceiptViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ReceiptViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_receipts, parent, false)
    )

    override fun getItemCount(): Int = receipts.size

    override fun onBindViewHolder(holder: ReceiptViewHolder, position: Int) {
        holder.bind(receipts[position], context)
    }

}
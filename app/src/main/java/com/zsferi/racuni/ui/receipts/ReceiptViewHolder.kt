package com.zsferi.racuni.ui.receipts

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.zsferi.racuni.vao.Receipt
import kotlinx.android.synthetic.main.item_receipts.view.*
import java.text.SimpleDateFormat
import java.util.*

class ReceiptViewHolder(view: View): RecyclerView.ViewHolder(view) {
    private val store = view.txtStore
    private val value = view.txtValue
    private val date = view.txtDate
    fun bind(receipt: Receipt, context: Context) {
        store.text = receipt.store
        val df = SimpleDateFormat("d. M. yyyy, hh:mm", Locale.US)
        date.text = df.format(receipt.date).toString()
        value.text = receipt.value.toString()
    }
}
package com.zsferi.racuni.ui.receipts


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.FirebaseFirestore

import com.zsferi.racuni.R
import com.zsferi.racuni.vao.Receipt
import kotlinx.android.synthetic.main.fragment_receipts.*
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class ReceiptsFragment : Fragment() {

    private lateinit var receiptsAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_receipts, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewManager = LinearLayoutManager(context)

        val db = FirebaseFirestore.getInstance()
        db.collection("receipts/").get()
            .addOnCompleteListener {
                if(it.isSuccessful) {
                    it.result!!.forEach {
                        val data = it.data
                        Log.println(Log.INFO, "theAppRacuni", data.toString())
                    }
                }
            }

        receiptsAdapter = ReceiptListAdapter(
            arrayListOf(
                Receipt(
                    Date(),
                    "NewYorker",
                    2.22
                )
            ), context!!
        )
        receipt_list.apply {
            layoutManager = viewManager
            adapter = receiptsAdapter
        }
    }

}

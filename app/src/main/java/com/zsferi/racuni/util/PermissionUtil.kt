package com.zsferi.racuni.util

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity

class PermissionUtil(private val activity:AppCompatActivity) {
    val READ_EXTERNAL_STORAGE = 1
    val WRITE_EXTERNAL_STORAGE = 2
    val COARSE_LOCATION_PERM = 3
    val FINE_LOCATION_PERM = 4
    val CAMERA_PERM = 5

    /* EXTERNAL STORAGE READ */
    fun checkReadExternalPermissions(): Boolean {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE)
            == PackageManager.PERMISSION_GRANTED) {
            return true
        }
        return false
    }

    fun requestReadPermissions() {
        ActivityCompat.requestPermissions(
            activity,
            arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE
            ),
            READ_EXTERNAL_STORAGE
        )
    }

    /* EXTERNAL STORAGE WRITE */
    fun checkWriteExternalPermissions(): Boolean {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            == PackageManager.PERMISSION_GRANTED) {
            return true
        }
        return false
    }

    fun requestWritePermissions() {
        ActivityCompat.requestPermissions(
            activity,
            arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ),
            WRITE_EXTERNAL_STORAGE
        )
    }

    /* CAMERA */
    fun checkCameraPermissions(): Boolean {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
            == PackageManager.PERMISSION_GRANTED) {
            return true
        }
        return false
    }

    fun requestCameraPermissions() {
        ActivityCompat.requestPermissions(
            activity,
            arrayOf(
                Manifest.permission.CAMERA
            ),
            CAMERA_PERM
        )
    }

    /* COARSE LOCATION */
    fun checkCoarseLocationPermissions(): Boolean {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION)
            == PackageManager.PERMISSION_GRANTED) {
            return true
        }
        return false
    }

    fun requestCoarseLocationPermissions() {
        ActivityCompat.requestPermissions(
            activity,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION
            ),
            COARSE_LOCATION_PERM
        )
    }

    /* FINE LOCATION */
    fun checkFineLocationPermissions(): Boolean {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED) {
            return true
        }
        return false
    }

    fun requestFineLocationPermissions() {
        ActivityCompat.requestPermissions(
            activity,
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            FINE_LOCATION_PERM
        )
    }
}
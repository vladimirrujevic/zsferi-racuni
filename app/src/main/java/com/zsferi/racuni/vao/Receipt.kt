package com.zsferi.racuni.vao

import java.util.*

data class Receipt(
    var date: Date,
    var store: String,
    var value: Double
)